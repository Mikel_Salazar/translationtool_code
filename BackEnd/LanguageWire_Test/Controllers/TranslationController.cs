﻿using LanguageWire_Test.Enums;
using LanguageWire_Test.Services;
using LanguageWire_Test.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageWire_Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TranslationController : ControllerBase
    {
        private readonly ITranslationService translationService;

        public TranslationController(ITranslationService translationService)
        {
            this.translationService = translationService;
        }
        /// <summary>
        /// Gets all the languages the tool can work with
        /// </summary>
        /// <returns>A list of available languages for translation</returns>
        [HttpGet("getLanguages")]
        public ActionResult<IEnumerable<string>> GetLanguages()
        {
            try
            {
                //test de devolver null y que casque y vaya por la exception
                return Ok(translationService.GetLanguages());
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }            
        }

        /// <summary>
        /// A method to translate a text from the source language to the target language
        /// </summary>
        /// <param name="translation"></param>
        /// <returns>A translation object with the translated text</returns>
        [HttpPost("translate")]
        public ActionResult<Translation> Translate([FromBody] Translation translation)
        {
            try
            {
                //test de que la translation sea null y casque y vaya por la exception
                translation.TranslatedText = translationService.Translate(translation);
                return Ok(translation);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
