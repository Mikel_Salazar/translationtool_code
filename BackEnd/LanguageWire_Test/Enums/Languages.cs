﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageWire_Test.Enums
{
    public enum Languages
    {
        en,
        fr,
        de
    }
}
