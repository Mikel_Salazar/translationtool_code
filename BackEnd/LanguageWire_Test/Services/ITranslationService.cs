﻿using LanguageWire_Test.Enums;
using LanguageWire_Test.ViewModels;
using System;
using System.Collections.Generic;

namespace LanguageWire_Test.Services
{
    public interface ITranslationService
    {
        string Translate(Translation translation);

        IEnumerable<string> GetLanguages();
    }
}