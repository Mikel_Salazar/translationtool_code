﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageWire_Test.ViewModels
{
    public class Translation
    {
        public string SourceLanguage { get; set; }
        public string TargetLanguage { get; set; }
        public string TextToTranslate { get; set; }
        public string TranslatedText { get; set; }
    }
}
