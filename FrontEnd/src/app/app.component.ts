import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslationService } from './services/translation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  languages: any;

  constructor(private translationService: TranslationService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getLanguages();
  }

  getLanguages() {
    return this.translationService.getLanguages().subscribe(response => {
      this.languages = response.body;
    }, (error) => {
      this._snackBar.open(`There has been a problem retrieving the languages: (${error.error})`, "Close");
    });
  }
}
