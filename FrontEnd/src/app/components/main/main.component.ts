import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Translation } from 'src/app/interfaces/translation';
import { TranslationService } from 'src/app/services/translation.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  @Input() languages: any;

  loading: boolean = false;

  translationForm = new FormGroup({
    sourceLanguage: new FormControl(''),
    targetLanguage: new FormControl(''),
    textToTranslate: new FormControl(''),
    translatedText: new FormControl(''),
  });

  constructor(private translationService: TranslationService, private _snackBar: MatSnackBar) { }

  onSubmit(formValues: any) {
    this.loading = true;
    let translation: Translation = formValues;
    this.translationService.translate(translation).subscribe(response => {
      let r: any = response.body;
      this.translationForm.controls["translatedText"].setValue(r.translatedText);
      this.loading = false;
    }, (error) => {
      console.log(error);
      
      this._snackBar.open(`There has been an error translating: (${error.error.detail})`, "Close");
      this.loading = false;
    });
  }

}
