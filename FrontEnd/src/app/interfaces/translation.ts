export interface Translation {
    sourceLanguage: string;
    targetLanguage: string;
    textToTranslate: string;
    translatedText: string;
}
