import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Translation } from '../interfaces/translation';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  constructor(private _http: HttpClient) { }

  getLanguages() {
    return this._http.get('http://localhost:5005/Translation/getLanguages', { observe: 'response' });
    //return this._http.get('https://localhost:44354/Translation/getLanguages', { observe: 'response' });
  }

  translate(translation: Translation) {
    return this._http.post('http://localhost:5005/Translation/translate', translation, { observe: 'response' });
    //return this._http.post('https://localhost:44354/Translation/translate', translation, { observe: 'response' });
  }
}
