# README #

This README explains how to get the LanguageWire Translation Tool up and running.

## Requeriments ##

**Docker** is needed to build and launch the app. Actually, the Docker Engine version is ``v20.10.8``

## How to launch the app ##

1. Download or clone this repo.
2. Open a terminal in the root directory (where the `docker-compose.yaml` file is located).
3. Run the following command: `docker-compose up`  
4. Once the app is built and running, open a browser and go to: `http://localhost:5006/`
	
## Usage ##
The use of the app is pretty stright forward:  

1. Choose the souce language from the select at the left.
2. Choose the target language from the select at the right.
3. Write or paste the text you want to translate in the left text area.
4. Click the big blue rounded button to receive the translation in the right text area.
	
	